package application

import (
	"context"
	"fmt"

	"github.com/go-kratos/kratos/v2/transport"
)

type IAppModule interface {
	ModuleName() string
	Install(core IApplication) error
	InitRouter(core IApplication) error
	Migrate(core IApplication) error
	Start(context.Context) error
	Stop(context.Context) error
	AddServer(key string, server transport.Server) error
	GetServer(key string) (transport.Server, error)
}

type AppModule struct {
	servers map[string]transport.Server
}

func (a AppModule) AddServer(key string, server transport.Server) error {
	if _, ok := a.servers[key]; ok {
		return fmt.Errorf("server %s is already exist", key)
	}
	a.servers[key] = server
	return nil
}

func (a AppModule) GetServer(key string) (transport.Server, error) {
	if server, ok := a.servers[key]; ok {
		return server, nil
	}
	return nil, fmt.Errorf("server %s is not exist", key)
}
