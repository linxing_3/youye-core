package service

import (
	"fmt"

	"gitee.com/linxing_3/youye-core/logger"
	"gitee.com/linxing_3/youye-core/storage"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type Context struct {
	Gin   *gin.Context
	Orm   *gorm.DB
	Msg   string
	MsgID string
	Log   *logger.Helper
	Error error
	Cache storage.AdapterCache
}

func (db *Context) AddError(err error) error {
	if db.Error == nil {
		db.Error = err
	} else if err != nil {
		db.Error = fmt.Errorf("%v; %w", db.Error, err)
	}
	return db.Error
}
