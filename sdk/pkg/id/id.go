package id

import (
	"fmt"
	"strconv"
	"time"
)

var sf *Sonyflake

func init() {
	var st Settings
	st.StartTime = time.Date(2021, 1, 1, 0, 0, 0, 0, time.UTC)
	///st.MachineID = EC2MachineID()
	sf = NewSonyflake(st)
	if sf == nil {
		panic("sonyflake not created")
	}
}

func GetNextID() string {
	id, err := sf.NextID()
	if err != nil {
		fmt.Println("id not generated")
	}
	return strconv.FormatUint(id, 10)
}

func GetNextIDNumber() uint64 {
	id, err := sf.NextID()
	if err != nil {
		fmt.Println("id not generated")
	}
	return id
}
