package structs_test

import (
	"encoding/json"
	"fmt"
	"testing"
)

type v struct {
	val string
}

func TestPtrMove(t *testing.T) {

	var val *v
	err := json.Unmarshal([]byte(`{"val":"test"}`), val)
	fmt.Println(val.val, err)
}
