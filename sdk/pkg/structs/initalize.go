package structs

func GetOrInit[T any](val *T, initfunc ...func() *T) *T {
	if val == nil {
		return initfunc[0]()
	}
	return val
}
