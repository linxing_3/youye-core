package structs

import "github.com/jinzhu/copier"

func MergeTo(from, to interface{}) error {
	return copier.Copy(to, from)
}
