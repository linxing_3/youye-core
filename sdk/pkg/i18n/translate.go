package i18n

import (
	ginI18n "github.com/gin-contrib/i18n"
	"github.com/gin-gonic/gin"
	"github.com/nicksnyder/go-i18n/v2/i18n"
)

type Args map[string]string

func NewArg() Args { return Args{} }
func (a Args) WithDefault(val string) Args {
	a["default"] = val
	return a
}
func (a Args) With(key, value string) Args {
	a[key] = value
	return a
}
func argToMap(arg Args) map[string]string {
	return arg
}

func Translate(context *gin.Context, messageID string, arg Args) string {
	msg, _ := ginI18n.GetMessage(context, &i18n.LocalizeConfig{
		MessageID:    messageID,
		TemplateData: argToMap(arg),
	})

	if msg == "" {
		msg = arg["default"]
	}

	return msg
}

func T(ctx *gin.Context, messageID string, arg Args) string {
	return Translate(ctx, messageID, arg)
}
