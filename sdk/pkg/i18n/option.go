package i18n

import (
	"golang.org/x/text/language"
)

type Option struct {
	Path       string   `json:"path" yaml:"path"`             // 语言文件存储路径
	Lngs       []string `json:"lngs" yaml:"lngs"`             // 支持的语言
	DefaultLng string   `json:"defaultLng" yaml:"defaultLng"` // 默认语言 ZH_cn
	FileType   string   `json:"fileType" yaml:"fileType"`     // 语言文件类型 yaml / json
	BindKey    string   `json:"bindKey" yaml:"bindKey"`       // 语言绑定的key
	BindFrom   string   `json:"bindFrom" yaml:"bindFrom"`     // 语言绑定的来源 header / query
}

func (o Option) GetLngTags() []language.Tag {
	tags := make([]language.Tag, 0, len(o.Lngs))
	for _, lang := range o.Lngs {
		tags = append(tags, language.Make(lang))
	}
	return tags
}
