package mq

import (
	"fmt"
	"strconv"
)

type MQEngine string

const (
	EngineRabbitMQ MQEngine = "rabbitmq"
	EngineNats     MQEngine = "nats"
)

type Option struct {
	Engine   MQEngine `json:"engine" yaml:"engine"`
	Host     string   `json:"host" yaml:"host"`
	Port     int      `json:"port" yaml:"port"`
	User     string   `json:"user" yaml:"user"`
	Pass     string   `json:"pass" yaml:"pass"`
	Vhost    string   `json:"vhost" yaml:"vhost"`
	Exchange string   `json:"exchange" yaml:"exchange"`
}

func (o Option) Dsn() string {
	switch o.Engine {
	case EngineRabbitMQ:
		return "amqp://" + o.User + ":" + o.Pass + "@" + o.Host + ":" + strconv.Itoa(o.Port) + "/" + o.Vhost
	case EngineNats:
		prefix := ""
		if o.User != "" && o.Pass != "" {
			prefix = o.User + ":" + o.Pass + "@"
		}
		return fmt.Sprintf("nats://%s%s:%d/", prefix, o.Host, o.Port)
	default:
		return ""
	}
}
