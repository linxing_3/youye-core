package mq

import (
	"context"

	"github.com/tx7do/kratos-transport/broker"
)

type IMQ interface {
	Subscribe(route string, handler broker.Handler) error
	Publish(ctx context.Context, route string, msg any) error
}
