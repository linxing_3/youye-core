package yerr

type ErrCode int

const (
	ErrCodeUnknown ErrCode = iota
	ErrCodeOk
)

// 参数有误
const (
	ErrCodeParamInvalid ErrCode = -10000 - iota // 参数有误
	ErrCodeParamMissing                         // 缺少参数

)

// 业务错误
const (
	ErrCodeBiz                 ErrCode = -20000 - iota // 业务错误
	ErrCodeInternalServerError                         // 服务器内部错误
	ErrCodeNotFound                                    // 未找到
	ErrCodeRepeated                                    // 重复操作

	ErrCodeUnauthed        ErrCode = -20100 - iota // 未授权
	ErrCodeUnauthedSession                         // 会话信息错误
)
