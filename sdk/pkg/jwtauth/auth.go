package jwtauth

import (
	"time"

	"gitee.com/linxing_3/youye-core/sdk/pkg/jwtauth/user"

	"github.com/gin-gonic/gin"
)

type LoginResponse struct {
	Code   int                    `json:"code"`            // 状态码
	Token  string                 `json:"token"`           // token
	Msg    string                 `json:"msg"`             // 提示内容
	Expire int                    `json:"expire"`          // 过期时间戳
	Third  map[string]interface{} `json:"third,omitempty"` // 第三方登录信息
}

type IAuthLoginHandler func(ctx *gin.Context) (user.ISessionInfo, error)

type IAuth interface {
	// 会话验证
	MiddlewareFunc() gin.HandlerFunc

	// 登录处理
	LoginHandler(ctx *gin.Context)

	// 更新session信息
	// @return string token
	UpdateSession(info user.ISessionInfo, expiredAt time.Time) (string, error)

	// 获取当前用户会话信息
	GetSessionInfo(ctx *gin.Context) (user.ISessionInfo, error)

	// 删除会话
	DeleteSession(ctx *gin.Context) error
}
