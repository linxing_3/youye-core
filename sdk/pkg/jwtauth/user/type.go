package user

type ISessionThird interface {
	GetOpenId() (string, bool)
}
type ISessionInfo interface {
	GetAppKey() string
	GetHost() string
	GetDeviceType() string
	GetUserId() string
	GetUsername() string
	GetNickName() string
	GetEmail() string
	GetPhone() string
	GetRoleId() string
	GetDeptId() string
	GetDeptPath() string
	GetTenantId() string
	GetSessionId() string

	GetThird() map[string]ISessionThird
	GetThirdPlatform(platform string) (ISessionThird, bool)
	SetThird(platform string, info ISessionThird) error

	LoadFromClaim(map[string]string) error
	GetClaim() map[string]string
}
