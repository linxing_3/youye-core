package database

import (
	"fmt"

	"gitee.com/linxing_3/youye-core/sdk/config"

	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func opens(driver string) func(string) gorm.Dialector {
	switch driver {
	case config.MySQL:
		return mysql.Open
	case config.Postgres:
		return postgres.Open
	case config.SQLite:
		return sqlite.Open
	}
	return nil
}

// NewDialect build dialect
func NewDialect(conf config.Database, conn gorm.ConnPool) (gorm.Dialector, error) {
	var dialect gorm.Dialector
	switch conf.Driver {
	case config.MySQL:
		dialect = newMySQLDialect(conn)
	case config.Postgres:
		dialect = newPostgresDialect(conn)
	case config.SQLite:
		dialect = newSQLiteDialect(conn)
	default:
		return nil, fmt.Errorf("unsupported database driver: %s", conf.Driver)
	}
	return dialect, nil
}

// newMySQLDialect build mysql dialect
func newMySQLDialect(conn gorm.ConnPool) gorm.Dialector {
	return mysql.New(mysql.Config{Conn: conn})
}

// newPostgresDialect build postgres dialect
func newPostgresDialect(conn gorm.ConnPool) gorm.Dialector {
	return postgres.New(postgres.Config{Conn: conn})
}

// newSQLiteDialect build sqlite dialect
func newSQLiteDialect(conn gorm.ConnPool) gorm.Dialector {
	return sqlite.Dialector{Conn: conn}
}
