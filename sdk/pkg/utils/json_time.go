package utils

import (
	"database/sql/driver"
	"fmt"
	"strings"
	"time"
)

func NewJsonTimeNow() *JSONTime         { return NewJsonTime(time.Now().Local()) }
func NewJsonTime(t time.Time) *JSONTime { return &JSONTime{Time: t} }
func NewJsonStr(t string) (*JSONTime, error) {
	n := &JSONTime{}
	err := n.UnmarshalJSON([]byte(t))
	return n, err
}

// JSONTime format json time field by myself
type JSONTime struct {
	time.Time
}

// MarshalJSON on JSONTime format Time field with %Y-%m-%d %H:%M:%S
func (t JSONTime) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("\"%s\"", t.Time.Format(time.DateTime))), nil
}
func (t *JSONTime) UnmarshalJSON(data []byte) (err error) {
	d := string(data)
	if len(d) > 0 && d[0] == '"' {
		d = strings.Trim(d, "\"")
	}
	now, _ := time.ParseInLocation(time.DateTime, d, time.Local)
	t.Time = now
	return
}

// Value insert timestamp into mysql need this function.
func (t JSONTime) Value() (driver.Value, error) {
	var zeroTime time.Time
	if t.Time.UnixNano() == zeroTime.UnixNano() {
		return nil, nil
	}
	return t.Time, nil
}

// Scan valueof time.Time
func (t *JSONTime) Scan(v interface{}) error {
	value, ok := v.(time.Time)
	if ok {
		*t = JSONTime{Time: value}
		return nil
	}
	return fmt.Errorf("can not convert %v to timestamp", v)
}
