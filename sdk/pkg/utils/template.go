package utils

import (
	"bytes"
	"text/template"
)

func ParseTemplate(filepath string, args interface{}) ([]byte, error) {
	tpl, err := template.ParseFiles(filepath)
	if err != nil {
		return nil, err
	}

	buf := bytes.Buffer{}
	err = tpl.Execute(&buf, args)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
