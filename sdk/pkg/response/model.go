package response

type DebugInfo struct {
	Err   string `swaggertype:"string"` // 错误信息
	Stack string `swaggertype:"string"` // 错误堆栈
}

var _ Responses = (*Response)(nil)

func NewResponse() Responses {
	return &Response{}
}

type Response struct {
	RequestId string      `json:"requestId,omitempty"` // 链路id
	Code      int         `json:"code,omitempty"`      // 状态码
	Msg       string      `json:"msg,omitempty"`       // 提示消息(可展示)
	Status    string      `json:"status,omitempty"`
	Data      interface{} `json:"data,omitempty"`  // 数据内容
	Debug     *DebugInfo  `json:"debug,omitempty"` // 调试模式下异常信息
}

type PageData struct {
	Total    int         `json:"total"`    // 总数
	PageNum  int         `json:"pageNum"`  // 当前页
	PageSize int         `json:"pageSize"` // 当前每页条数
	List     interface{} `json:"list"`     // 数据列表
}

func (r *Response) SetTraceID(requestId string) Responses {
	r.RequestId = requestId
	return r
}
func (r *Response) SetCode(code int) Responses {
	r.Code = code
	return r
}

func (r *Response) SetMsg(msg string) Responses {
	r.Msg = msg
	return r
}
func (r *Response) SetSuccess(success bool) Responses {
	if success {
		r.Status = "success"
	} else {
		r.Status = "error"
	}
	return r
}
func (r *Response) SetError(err error) Responses {
	r.Debug = &DebugInfo{
		Err: err.Error(),
	}
	return r
}

func (r *Response) SetData(data interface{}) Responses {
	r.Data = data
	return r
}
