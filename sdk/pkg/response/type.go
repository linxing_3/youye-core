/*
 * @Author: lwnmengjing
 * @Date: 2021/6/8 5:51 下午
 * @Last Modified by: lwnmengjing
 * @Last Modified time: 2021/6/8 5:51 下午
 */

package response

type Responses interface {
	SetCode(int) Responses
	SetTraceID(string) Responses
	SetMsg(string) Responses
	SetData(interface{}) Responses
	SetSuccess(bool) Responses
	SetError(err error) Responses
}

type IRespWriter interface {
	GetHeader(key string) string
	SetHeader(key string, val string)
	Set(key string, val any)
	Get(key string) (val any, exists bool)
	AbortWithStatusJSON(code int, obj any)
}
