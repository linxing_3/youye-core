package gin

import (
	"gitee.com/linxing_3/youye-core/sdk/pkg/response"
	"github.com/gin-gonic/gin"
)

var _ response.IRespWriter = (*GinWriter)(nil)

func NewGinRespWriter(gin *gin.Context) *GinWriter {
	return &GinWriter{gin: gin}
}

type GinWriter struct {
	gin *gin.Context
}

// AbortWithStatusJSON implements response.IRespWriter.
func (g *GinWriter) AbortWithStatusJSON(code int, obj any) {
	g.gin.AbortWithStatusJSON(code, obj)
}

// Get implements response.IRespWriter.
func (g *GinWriter) Get(key string) (val any, exists bool) {
	return g.gin.Get(key)
}

// GetHeader implements response.IRespWriter.
func (g *GinWriter) GetHeader(key string) string {
	return g.gin.GetHeader(key)
}

// Set implements response.IRespWriter.
func (g *GinWriter) Set(key string, val any) {
	g.gin.Set(key, val)
}

// SetHeader implements response.IRespWriter.
func (g *GinWriter) SetHeader(key string, val string) {
	g.gin.Header(key, val)
}
