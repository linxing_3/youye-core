package config

type HttpServer struct {
	Host       string   `yaml:"host"`       // 主机
	Port       int64    `yaml:"port"`       // 端口
	WhiteList  []string `yaml:"whiteList"`  // 白名单
	ExceptList []string `yaml:"exceptList"` // 黑名单
	TLS        *Ssl     `yaml:"tls"`        // TLS
}

var HttpServerConfig = new(HttpServer)
