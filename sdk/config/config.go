package config

import (
	"fmt"
	"log"

	"gitee.com/linxing_3/youye-core/config"
	"gitee.com/linxing_3/youye-core/config/source"
	"gitee.com/linxing_3/youye-core/sdk/pkg/i18n"
)

var (
	ExtendConfig interface{}
	_cfg         *Settings
)

// Settings 兼容原先的配置结构
type Settings struct {
	Settings  Config `yaml:"settings"`
	callbacks []func()
}

func (e *Settings) runCallback() {
	for i := range e.callbacks {
		e.callbacks[i]()
	}
}

func (e *Settings) OnChange() {
	e.init()
	log.Println("config change and reload")
}

func (e *Settings) Init() {
	e.init()
	log.Println("config init")
}

func (e *Settings) init() {
	e.Settings.Logger.Setup()
	e.Settings.multiDatabase()
	e.runCallback()
}

// Config 配置集合
type Config struct {
	Application *Application          `yaml:"application"`
	Http        *HttpServer           `yaml:"http"`
	Grpc        *GrpcServer           `yaml:"grpc"`
	Logger      *Logger               `yaml:"logger"`
	Jwt         *Jwt                  `yaml:"jwt"`
	Registry    *Etcd                 `yaml:"registry"`
	Database    *Database             `yaml:"database"`
	Databases   *map[string]*Database `yaml:"databases"`
	Gen         *Gen                  `yaml:"gen"`
	Cache       *Cache                `yaml:"cache"`
	Locker      *Locker               `yaml:"locker"`
	Extend      interface{}           `yaml:"extend"`
	I18n        *i18n.Option          `yaml:"i18n"`
}

// 多db改造
func (e *Config) multiDatabase() {
	if len(*e.Databases) == 0 {
		*e.Databases = map[string]*Database{
			"*": e.Database,
		}

	}
}

// Setup 载入配置文件
func Setup(s []source.Source,
	fs ...func()) error {
	_cfg = &Settings{
		Settings: Config{
			Application: ApplicationConfig,
			Http:        HttpServerConfig,
			Grpc:        GrpcServerConfig,
			Logger:      LoggerConfig,
			Jwt:         JwtConfig,
			Database:    DatabaseConfig,
			Registry:    EtcdConfig,
			Databases:   &DatabasesConfig,
			Gen:         GenConfig,
			Cache:       CacheConfig,
			Locker:      LockerConfig,
			Extend:      ExtendConfig,
			I18n:        I18NConfig,
		},
		callbacks: fs,
	}
	var err error
	config.DefaultConfig, err = config.NewConfig(
		config.WithSource(s...),
		config.WithEntity(_cfg),
	)
	if err != nil {
		log.Fatal(fmt.Sprintf("New config object fail: %s", err.Error()))
		return err
	}
	_cfg.Init()
	return nil
}
func GetConfig() *Config {
	return &_cfg.Settings
}
