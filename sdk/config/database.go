package config

const (
	MySQL    = "mysql"
	Postgres = "postgres"
	SQLite   = "sqlite"
)

type Database struct {
	Driver          string             `yaml:"driver"`
	Source          string             `yaml:"source"`
	ConnMaxIdleTime int                `yaml:"connMaxIdleTime"`
	ConnMaxLifeTime int                `yaml:"connMaxLifeTime"`
	MaxIdleConns    int                `yaml:"maxIdleConns"`
	MaxOpenConns    int                `yaml:"maxOpenConns"`
	AutoMigrate     bool               `yaml:"autoMigrate"`
	Registers       []DBResolverConfig `yaml:"registers"`
	Logger          *Logger            `yaml:"logger"`
}

type DBResolverConfig struct {
	Sources  []string `yaml:"sources"`
	Replicas []string `yaml:"replicas"`
	Policy   string   `yaml:"policy"`
	Tables   []string `yaml:"tables"`
}

var (
	DatabaseConfig  = new(Database)
	DatabasesConfig = make(map[string]*Database)
)
