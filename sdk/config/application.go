package config

import "strings"

type Application struct {
	ReadTimeout   int    `yaml:"readTimeout"`
	WriterTimeout int    `yaml:"writerTimeout"`
	Name          string `yaml:"name"`
	Mode          string `yaml:"mode"`
	DemoMsg       string `yaml:"demoMsg"`
	EnableDP      bool   `yaml:"enableDP"`
}

func (a Application) IsDebug() bool { return strings.ToLower(a.Mode) == "debug" }

var ApplicationConfig = new(Application)
