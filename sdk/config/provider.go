package config

import (
	"gitee.com/linxing_3/youye-core/sdk/pkg/i18n"
	"github.com/google/wire"
)

var Provider = wire.NewSet(
	InstanceApplication,
	InstanceCache,
	InstanceDatabase,
	InstanceGen,
	IntanceJwt,
	InstanceLocker,
	InstanceLogger,
	InstanceSSL,
	InstanceI18N,
)

func InstanceApplication() *Application { return ApplicationConfig }
func InstanceCache() *Cache             { return CacheConfig }
func InstanceDatabase() *Database       { return DatabaseConfig }
func InstanceGen() *Gen                 { return GenConfig }
func IntanceJwt() *Jwt                  { return JwtConfig }
func InstanceLocker() *Locker           { return LockerConfig }
func InstanceLogger() *Logger           { return LoggerConfig }
func InstanceSSL() *Ssl                 { return SslConfig }
func InstanceI18N() *i18n.Option        { return I18NConfig }
func InstanceHttp() *HttpServer         { return HttpServerConfig }
func InstanceGrpc() *GrpcServer         { return GrpcServerConfig }
