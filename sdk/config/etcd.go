package config

type Etcd struct {
	Endpoints []string `yaml:"endpoints" json:"endpoints"`
}

var EtcdConfig = new(Etcd)
