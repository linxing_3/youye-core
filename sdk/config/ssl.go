package config

type Ssl struct {
	KeyStr string `yaml:"keyStr"`
	Pem    string `yaml:"pem"`
	Enable bool   `yaml:"enable"`
	Domain string `yaml:"domain"`
}

var SslConfig = new(Ssl)
