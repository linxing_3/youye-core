module gitee.com/linxing_3/youye-core/plugins/logger/logrus

go 1.18

require (
	gitee.com/linxing_3/youye-core v1.3.11
	github.com/sirupsen/logrus v1.8.0
)

require (
	github.com/magefile/mage v1.10.0 // indirect
	golang.org/x/sys v0.0.0-20210603081109-ebe580a85c40 // indirect
)

//replace gitee.com/linxing_3/youye-core => ../../../
