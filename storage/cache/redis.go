package cache

import (
	"context"
	"encoding/json"
	"time"

	"gitee.com/linxing_3/youye-core/storage"

	"github.com/go-redis/redis/v9"
	"github.com/spf13/cast"
)

// NewRedis redis模式
func NewRedis(client *redis.Client, options *redis.Options) (*Redis, error) {
	if client == nil {
		client = redis.NewClient(options)
	}
	r := &Redis{
		client: client,
	}
	err := r.connect()
	if err != nil {
		return nil, err
	}
	return r, nil
}

var _ storage.AdapterCache = (*Redis)(nil)

// Redis cache implement
type Redis struct {
	client *redis.Client
}

func (*Redis) String() string {
	return "redis"
}

// connect connect test
func (r *Redis) connect() error {
	var err error
	_, err = r.client.Ping(context.TODO()).Result()
	return err
}

// Get from key
func (r *Redis) Get(key string) (string, error) {
	return r.client.Get(context.TODO(), key).Result()
}

// Set value with key and expire time
func (r *Redis) Set(key string, val interface{}, expire int) error {
	return r.client.Set(context.TODO(), key, val, time.Duration(expire)*time.Second).Err()
}

// Del delete key in redis
func (r *Redis) Del(key string) error {
	return r.client.Del(context.TODO(), key).Err()
}

func (r *Redis) AddList(key string, values ...interface{}) error {
	return r.client.LPush(context.TODO(), key, values...).Err()
}

func (r *Redis) Append(key string, values ...interface{}) error {
	return r.client.RPush(context.TODO(), key, values...).Err()
}

func (r *Redis) GetList(key string) ([]string, error) {
	return r.client.LRange(context.TODO(), key, 0, -1).Result()
}

func (r *Redis) GetPrefix(key string, length ...int) ([]string, error) {
	stopAt := int(0)
	if len(length) > 0 {
		stopAt = length[0]
	}
	return r.client.LPopCount(context.TODO(), key, stopAt).Result()
}

func (r *Redis) HashGetAll(hk string) (map[string]string, error) {
	return r.client.HGetAll(context.TODO(), hk).Result()
}

func (r *Redis) HashSetField(hk, key string, value interface{}) error {
	return r.client.HSetNX(context.TODO(), hk, key, value).Err()
}

func (r *Redis) HashSet(hk string, value interface{}, expire int) error {

	var mapval interface{}
	switch vt := value.(type) {
	case map[interface{}]interface{}, map[string]interface{}, string:
		mapval = cast.ToStringMap(vt)
	case map[string]string, map[string]int, map[string]bool, map[string]float64:
		mapval = vt
	default:
		jbts, err := json.Marshal(value)
		if err != nil {
			return err
		}
		mapval, err = cast.ToStringMapE(string(jbts))
		if err != nil {
			return err
		}
	}
	if err := r.client.HSet(context.TODO(), hk, mapval).Err(); err != nil {
		return err
	}
	return nil
}

// HashGet from key
func (r *Redis) HashGet(hk, key string) (string, error) {
	return r.client.HGet(context.TODO(), hk, key).Result()
}

// HashDel delete key in specify redis's hashtable
func (r *Redis) HashDel(hk, key string) error {
	return r.client.HDel(context.TODO(), hk, key).Err()
}

// Increase
func (r *Redis) Increase(key string) error {
	return r.client.Incr(context.TODO(), key).Err()
}

func (r *Redis) Decrease(key string) error {
	return r.client.Decr(context.TODO(), key).Err()
}

// Set ttl
func (r *Redis) Expire(key string, dur time.Duration) error {
	return r.client.Expire(context.TODO(), key, dur).Err()
}

// GetClient 暴露原生client
func (r *Redis) GetClient() *redis.Client {
	return r.client
}
