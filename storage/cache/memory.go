package cache

import (
	"fmt"
	"strconv"
	"sync"
	"time"

	"gitee.com/linxing_3/youye-core/storage"

	"github.com/spf13/cast"
)

type item struct {
	Value   string
	Expired time.Time
}

// NewMemory memory模式
func NewMemory() *Memory {
	return &Memory{
		items: new(sync.Map),
	}
}

var _ storage.AdapterCache = (*Memory)(nil)

type Memory struct {
	items *sync.Map
	mutex sync.RWMutex
}

func (*Memory) String() string {
	return "memory"
}

func (m *Memory) connect() {
}

func (m *Memory) Get(key string) (string, error) {
	item, err := m.getItem(key)
	if err != nil || item == nil {
		return "", err
	}
	return item.Value, nil
}

func (m *Memory) getItem(key string) (*item, error) {
	var err error
	i, ok := m.items.Load(key)
	if !ok {
		return nil, nil
	}
	switch i.(type) {
	case *item:
		item := i.(*item)
		if item.Expired.Before(time.Now()) {
			//过期
			_ = m.del(key)
			//过期后删除
			return nil, nil
		}
		return item, nil
	default:
		err = fmt.Errorf("value of %s type error", key)
		return nil, err
	}
}

func (m *Memory) Set(key string, val interface{}, expire int) error {
	s, err := cast.ToStringE(val)
	if err != nil {
		return err
	}
	item := &item{
		Value:   s,
		Expired: time.Now().Add(time.Duration(expire) * time.Second),
	}
	return m.setItem(key, item)
}

func (m *Memory) setItem(key string, item *item) error {
	m.items.Store(key, item)
	return nil
}

func (m *Memory) Del(key string) error {
	return m.del(key)
}

func (m *Memory) del(key string) error {
	m.items.Delete(key)
	return nil
}

func (m *Memory) AddList(key string, values ...interface{}) error {
	return m.Set(key, values, 0)
}

func (m *Memory) GetList(key string) ([]string, error) {
	item, err := m.getItem(key)
	if err != nil || item == nil {
		return nil, err
	}
	return cast.ToStringSliceE(item.Value)
}

func (m *Memory) Append(key string, values ...interface{}) error {
	item, err := m.getItem(key)
	if err != nil || item == nil {
		return err
	}
	list, err := cast.ToStringSliceE(item.Value)
	if err != nil {
		return err
	}
	list = append(list, cast.ToStringSlice(values)...)
	return m.Set(key, list, 0)
}

func (m *Memory) GetPrefix(key string, length ...int) ([]string, error) {
	vals, err := m.GetList(key)
	if err != nil {
		return []string{}, err
	}
	aimLen := 0
	if len(length) > 0 {
		aimLen = int(length[0])
	}
	if aimLen < len(vals) {
		return vals[:aimLen], nil
	}
	return vals, nil
}

func (m *Memory) HashSet(hk string, value interface{}, expire int) error {
	return m.Set(hk, value, expire)
}

func (m *Memory) HashSetField(hk, key string, value interface{}) error {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	i, err := m.getItem(hk)
	if err != nil {
		return err
	}
	if i == nil {
		i = &item{
			Value: "{}",
		}
	}
	mm, err := cast.ToStringMapStringE(i.Value)
	if err != nil {
		return err
	}
	mm[key] = cast.ToString(value)
	i.Value = cast.ToString(mm)
	return m.setItem(hk, i)
}

func (m *Memory) HashGetAll(hk string) (map[string]string, error) {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	item, err := m.getItem(hk)
	if err != nil || item == nil {
		return nil, err
	}
	return cast.ToStringMapStringE(item.Value)
}

func (m *Memory) HashGet(hk, key string) (string, error) {
	item, err := m.getItem(hk + key)
	if err != nil || item == nil {
		return "", err
	}
	return item.Value, err
}

func (m *Memory) HashDel(hk, key string) error {
	return m.del(hk + key)
}

func (m *Memory) Increase(key string) error {
	return m.calculate(key, 1)
}

func (m *Memory) Decrease(key string) error {
	return m.calculate(key, -1)
}

func (m *Memory) calculate(key string, num int) error {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	item, err := m.getItem(key)
	if err != nil {
		return err
	}

	if item == nil {
		err = fmt.Errorf("%s not exist", key)
		return err
	}
	var n int
	n, err = cast.ToIntE(item.Value)
	if err != nil {
		return err
	}
	n += num
	item.Value = strconv.Itoa(n)
	return m.setItem(key, item)
}

func (m *Memory) Expire(key string, dur time.Duration) error {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	item, err := m.getItem(key)
	if err != nil {
		return err
	}
	if item == nil {
		err = fmt.Errorf("%s not exist", key)
		return err
	}
	item.Expired = time.Now().Add(dur)
	return m.setItem(key, item)
}
