package logger

import (
	"context"
	"testing"
	"time"

	logCore "gitee.com/linxing_3/youye-core/logger"
	"gorm.io/gorm/logger"
)

func TestNew(t *testing.T) {
	l := New(logger.Config{
		SlowThreshold: time.Second,
		Colorful:      true,
		LogLevel: logger.LogLevel(
			logCore.DefaultLogger.Options().Level.LevelForGorm()),
	}, nil)
	l.Info(context.TODO(), "test")
}
